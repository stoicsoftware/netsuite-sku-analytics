if (typeof define !== "function") {
    var define = require("amdefine")(module);
}

define([
    "N/search",
    "../lib/ramda.min",
    "../lib/moment.min",
    "../lib/sa-DateUtilities"
], function (s, R, moment, d) {

    /**
     * Business logic and data manipulation methods for the Total Monthly
     * Sales data
     *
     * @exports sa/total-monthly
     *
     * @requires N/search
     * @requires ramda
     * @requires moment
     * @requires sa/date-util
     *
     * @copyright 2016 Stoic Software
     * @author Eric T Grubaugh <eric@stoic.software>
     *
     * @NApiVersion 2.x
     * @NModuleScope Public
     */
    var exports = {};

    /**
     * The data format used to define Total Monthly Sales data
     * @typedef {Object} MonthlySalesData
     * @property month {moment} A moment representing the first of the month
     * @property sales {Number} The number of sales in the month
     */

    /**
     * monthToData :: moment -> MonthlySalesData
     *
     * Translates a moment representing a month in the year into a processable
     * data object
     *
     * @governance 0
     *
     * @param m {moment} The moment to translate
     *
     * @return {MonthlySalesData} Translated moment
     *
     * @private
     * @function monthToData
     */
    function monthToData(m) {
        return {
            "month": m,
            "sales": 0
        };
    }

    /**
     * monthsToData :: [moment] -> [MonthlySalesData]
     *
     * Translates a list of moments representing a month in the year into a
     * list of processable data objects
     *
     * @governance 0
     *
     * @param data {moment[]} The moments to translate
     *
     * @return {MonthlySalesData[]} Translated moments
     *
     * @private
     * @function monthsToData
     */
    var monthsToData = R.map(monthToData);

    /**
     * resultToData :: search.Result -> MonthlySalesData
     *
     * Translates a Total Monthly Sales search result into a processable
     * data object
     *
     * @governance 0
     *
     * @param result {search.Result} The search result to translate
     *
     * @return {MonthlySalesData} Translated search result
     *
     * @private
     * @function resultToData
     */
    function resultToData(result) {
        var month = moment(
            result.getValue({
                "name": "formulatext",
                "summary": s.Summary.GROUP
            }),
            "MMMM YYYY"
        );

        return {
            "month": month.startOf("month"),
            "sales": result.getValue({
                "name": "quantity",
                "summary": s.Summary.SUM
            })
        };
    }

    /**
     * resultsToData :: [search.Result] -> [MonthlySalesData]
     *
     * Translates a list of Total Monthly Sales search results into a list of
     * processable data object
     *
     * @governance 0
     *
     * @param data {search.Result[]} The list of search results to translate
     *
     * @return {MonthlySalesData[]} List of Monthly Sales data objects
     *
     * @private
     * @function resultsToData
     */
    var resultsToData = R.map(resultToData);

    /**
     * Predicate that determines whether two SKU sales data elements should be
     * merged together
     *
     * @param a {MonthlySalesData} first SKU sales data
     * @param b {MonthlySalesData} second SKU sales data
     *
     * @return {Boolean}
     *
     * @private
     * @function areDuplicates
     */
    function areDuplicates(a, b) {
        return d.sameMonth(a.month, b.month);
    }

    /**
     * generateData :: [search.Result] -> moment -> [MonthlySalesData]
     *
     * Translates the given search results to Monthly Sales data for the
     * rolling year starting with the given date
     *
     * @governance 0
     *
     * @param results {search.Result[]} List of Total Monthly Sales search
     *     results to translate into processable data Objects
     * @param seedDate {moment} The date to start the rolling year from
     *
     * @return {MonthlySalesData[]} Data list processable by the Total Monthly
     *     module
     *
     * @private
     * @function generateData
     */
    var generateData = R.useWith(R.unionWith(areDuplicates), [
        resultsToData,
        R.pipe(
            d.monthsSameYear,
            monthsToData
        )
    ]);

    /**
     * translateResults :: [search.Result] -> [MonthlySalesData]
     *
     * Translates the given search results to Monthly Sales data
     *
     * @governance 0
     *
     * @param results {search.Result[]} List of Total Monthly Sales search
     *     results to translate into processable data Objects. Results
     *     must be grouped by a formulatext column representing the month and
     *     contain a summed quantity column representing the sales for that
     *     month
     *
     * @return {MonthlySalesData[]} Data list processable by the Total Monthly
     *     module
     *
     * @static
     * @function translateResults
     */
    var translateResults = R.pipe(
        generateData(R.__, moment()),
        R.sortBy(R.pipe(
            R.prop("month"),
            R.invoker(0, "valueOf")
        ))
    );

    /**
     * label :: MonthlySalesData -> String
     *
     * Generates a label for a MonthlySalesData object that can be used on, for
     * instance, a chart axis
     *
     * @governance 0
     *
     * @param data {MonthlySalesData} The data object to generate a label for
     *
     * @return {String} The label for the given data object
     *
     * @private
     * @function label
     */
    var label = R.pipe(
        R.prop("month"),
        d.format("MMM YYYY")
    );

    /**
     * labels :: [MonthlySalesData] -> [String]
     *
     * Generates the list of axis labels for the given dataset
     *
     * @governance 0
     *
     * @param data {MonthlySalesData[]} The dataset to transform to labels
     *
     * @return {String[]} The list of labels to be used for the data
     *
     * @static
     * @function labels
     */
    var labels = R.map(label);

    /**
     * values :: [MonthlySalesData] -> [Number]
     *
     * Generates the values that will be charted for the given dataset
     *
     * @governance 0
     *
     * @param data {MonthlySalesData[]} The dataset to transform to values
     *
     * @return {Number[]} The values to be used for the chart
     *
     * @static
     * @function values
     */
    var values = R.pluck("sales");

    exports.values = values;
    exports.labels = labels;
    exports.translateResults = translateResults;
    return exports;
});
