define(["N/ui/serverWidget", "N/search", "N/util", "N/runtime"], function (ui, s, util, runtime) {

    /**
     * Renders the Portlet for best-selling SKUs by units sold
     *
     * @exports sa/best-sellers/pl
     *
     * @requires N/ui/serverWidget
     * @requires N/search
     * @requires N/util
     * @requires N/runtime
     *
     * @copyright 2016 Stoic Software
     * @author Eric T Grubaugh <eric@stoic.software>
     *
     * @NApiVersion 2.x
     * @NModuleScope Public
     * @NScriptType Portlet
     */
    var exports = {};

    /**
     * <code>render</code> event handler
     *
     * @governance 10
     *
     * @param context
     *        {Object}
     * @param context.portlet
     *        {Portlet} The portlet object used for rendering.
     * @param context.column
     *        {Number} The column index for the portlet on the
     *            dashboard. Use one of the following numeric values:
     *            <ol>
     *            <li>left column</li>
     *            <li>center column</li>
     *            <li>right column</li>
     *            </ol>
     *
     * @return {void}
     *
     * @static
     * @function render
     */
    function render(context) {
        var p = context.portlet;
        var script = runtime.getCurrentScript();

        p.title = script.getParameter({"name": "custscript_sa_bestsellers_title"});
        populate(list(p), skuData(
            script.getParameter({"name": "custscript_sa_bestsellers_search"}),
            script.getParameter({"name": "custscript_sa_bestsellers_rows"})
        ));
    }

    /**
     * Retrieves best-selling SKU data
     *
     * @governance 10
     *
     * @param searchId {Number} The internal ID of the Best-Selling SKU Saved Search to execute
     * @param count {Number} The number of results to retrieve
     *
     * @return {search.Result[]} Best-selling SKU Search Result list
     */
    function skuData(searchId, count) {
        return s.load({"id": searchId})
            .run()
            .getRange({
                "start": 0,
                "end": count
            });
    }

    /**
     * Defines the list that will display SKU search results
     *
     * @governance 0
     *
     * @param p {Portlet} Reference to the Portlet that will contain the list
     *
     * @return {Portlet} The Portlet with List Columns added
     *
     * @private
     * @function list
     */
    function list(p) {
        p.addColumn({
            "id": "item",
            "label": "SKU",
            "type": ui.FieldType.TEXT
        });
        p.addColumn({
            "id": "quantity",
            "label": "Units Sold",
            "type": ui.FieldType.FLOAT,
            "align": ui.LayoutJustification.RIGHT
        });
        p.addColumn({
            "id": "percenttotal",
            "label": "% of Total Units Sold",
            "type": ui.FieldType.PERCENT,
            "align": ui.LayoutJustification.RIGHT
        });
        return p;
    }

    /**
     * Populates the given List Portlet with the provided SKU search results
     *
     * @governance 0
     *
     * @param p {Portlet} Reference to the List Portlet to populate
     * @param data {search.Result} Best-selling SKU search results
     *
     * @return {void}
     *
     * @private
     * @function populate
     */
    function populate(p, data) {
        util.each(data, function (r) {
            p.addRow({
                // TODO Direct index access here seems a little magic; improve?
                "row": {
                    "item": r.getText(r.columns[0]),
                    "quantity": r.getValue(r.columns[1]),
                    "percenttotal": r.getValue(r.columns[2])
                }
            });
        });
    }

    exports.render = render;

    return exports;
});
