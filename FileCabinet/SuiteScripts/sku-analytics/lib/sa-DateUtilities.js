if (typeof define !== "function") {
    var define = require("amdefine")(module);
}

define(["./ramda.min"], function (R) {

    /**
     * Common date utility methods used by SKU Analytics
     *
     * @exports sa/date-util
     *
     * @requires ramda
     *
     * @copyright 2016 Stoic Software
     * @author Eric T Grubaugh <eric@stoic.software>
     *
     * @NApiVersion 2.x
     * @NModuleScope Public
     */
    var exports = {};

    /**
     * String -> moment -> String
     *
     * Formats a moment using the provided date format string
     *
     * @governance 0
     *
     * @param format {String} The date format
     * @param date {moment} The moment to format
     *
     * @return {String} The formatted moment
     *
     * @static
     * @function format
     *
     * @see [moment#format]{@link http://momentjs.com/docs/#/displaying/format/}
     */
    var format = R.invoker(1, "format");

    var goBack = R.invoker(2, "subtract");
    var goForward = R.invoker(2, "add");

    var previous = goBack(1);
    var previousMonth = previous("month");

    var next = goForward(1);
    var nextMonth = next("month");

    var same = R.invoker(2, "isSame");
    var sameMonth = same(R.__, "month");

    /**
     * Iterator function that generates a sequence of moments for a single
     * calendar year starting at the given <code>start</code>
     *
     * @governance 0
     *
     * @param start {moment} The seed moment for the sequence
     * @param date {moment} The current date in the sequence
     *
     * @return {moment[]|Boolean} <code>false</code> if iteration should stop,
     *     otherwise a pair of moments. The first moment is added to the
     *     sequence, and the second is the seed value for the next iteration
     *
     * @private
     * @function yearGenerator
     */
    var yearGenerator = R.curryN(2, function (start, date) {
        var startMonth = start.clone().startOf("month");
        var endMonth = previous("year")(startMonth);
        var currentMonth = date.clone().startOf("month");

        return currentMonth.isSameOrBefore(endMonth) ?
            false :
            [currentMonth, previousMonth(currentMonth.clone())];
    });

    /**
     * monthsSameYear :: moment -> [moment]
     *
     * Generates a list of moments representing each of the months in the
     * same rolling calendar year as the given date
     *
     * @governance 0
     *
     * @param seedDate {moment} The seed value for generating a sequence of
     *     months
     *
     * @return {moment[]} A list of moments representing each month in the
     *     same rolling year as seedDate
     *
     * @static
     * @function monthsSameYear
     */
    function monthsSameYear(seedDate) {
        return R.unfold(yearGenerator(seedDate))(seedDate);
    }

    exports.goBack = goBack;
    exports.goForward = goForward;
    exports.format = format;
    exports.monthsSameYear = monthsSameYear;
    exports.next = next;
    exports.nextMonth = nextMonth;
    exports.previous = previous;
    exports.previousMonth = previousMonth;
    exports.same = same;
    exports.sameMonth = sameMonth;
    return exports;
});
