define([
    "N/search",
    "../lib/ramda.min",
    "../lib/moment.min",
    "../lib/sa-DateUtilities"
], function (s, R, moment, d) {

    /**
     * Business logic and data manipulation methods for the Total Monthly
     * Sales by SKU data
     *
     * @exports sa/total-monthly-by-sku
     *
     * @requires N/search
     * @requires ramda
     * @requires moment
     * @requires sa/date-util
     *
     * @copyright 2016 Stoic Software
     * @author Eric T Grubaugh <eric@stoic.software>
     *
     * @NApiVersion 2.x
     * @NModuleScope Public
     */
    var exports = {};

    /**
     * The data format used to define Total Monthly Sales by SKU data
     *
     * @typedef {Object} MonthlySalesBySkuData
     *
     * @property itemId {Number} Internal ID of the SKU
     * @property itemName {String} Display name of the SKU
     * @property month {moment} A moment representing the first of the month
     * @property sales {Number} The number of sales in the month for the SKU
     */

    /**
     * The current timestamp at the time this module is loaded
     *
     * @type {moment}
     *
     * @private
     * @property now
     */
    var now = R.always(moment());

    /**
     * monthToData :: moment -> MonthlySalesBySkuData
     *
     * Translates a moment representing a month in the year into a processable
     * data object
     *
     * @governance 0
     *
     * @param m {moment} The moment to translate
     *
     * @return {MonthlySalesBySkuData} Translated moment
     *
     * @private
     * @function monthToData
     */
    function monthToData(m) {
        return {
            "month": m,
            "sales": 0
        };
    }

    /**
     * monthsToData :: [moment] -> [MonthlySalesBySkuData]
     *
     * Translates a list of moments representing a month in the year into a
     * list of processable data objects
     *
     * @governance 0
     *
     * @param data {moment[]} The moments to translate
     *
     * @return {MonthlySalesBySkuData[]} Translated moments
     *
     * @private
     * @function monthsToData
     */
    var monthsToData = R.map(monthToData);

    /**
     * generateYear :: moment -> [MonthlySalesBySkuData]
     *
     * @governance 0
     *
     * @param seedDate {moment} The moment to seed year generation
     *
     * @return {MonthlySalesBySkuData[]} Full year of empty sales data
     *
     * @private
     * @function generateYear
     */
    var generateYear = R.pipe(
        d.monthsSameYear,
        monthsToData
    );

    var currentRollingYear = generateYear(now());

    /**
     * emptyYear :: [MonthlySalesBySkuData] -> [MonthlySalesBySkuData]
     *
     * Accepts a list of Monthly Sales by SKU data and ensures that a value
     * exists for every month in the current rolling year
     *
     * @governance 0
     *
     * @param data {MonthlySalesBySkuData[]} The Monthly Sales by SKU data to
     *     fill out
     *
     * @return {MonthlySalesBySkuData[]} A full year of Monthly Sales by SKU
     *     data
     *
     * @private
     * @function emptyYear
     */
    var emptyYear = R.flip(
        R.useWith(R.map, [
            R.pipe(R.head, R.merge),
            R.identity
        ])
    )(currentRollingYear);

    /**
     * resultToData :: search.Result -> MonthlySalesBySkuData
     *
     * Translates a Total Monthly Sales by SKU search result into a processable
     * data object
     *
     * @governance 0
     *
     * @param result {search.Result} The search result to translate
     *
     * @return {MonthlySalesBySkuData} Translated search result
     *
     * @private
     * @function resultToData
     */
    function resultToData(result) {
        var month = moment(
            result.getValue({
                "name": "formulatext",
                "summary": s.Summary.GROUP
            }),
            "MMMM YYYY"
        );

        return {
            "itemId": result.getValue({
                "name": "item",
                "summary": s.Summary.GROUP
            }),
            "itemName": result.getText({
                "name": "item",
                "summary": s.Summary.GROUP
            }),
            "month": month.startOf("month"),
            "sales": parseFloat(result.getValue({
                "name": "quantity",
                "summary": s.Summary.SUM
            }))
        };
    }

    /**
     * resultsToData :: [search.Result] -> [MonthlySalesBySkuData]
     *
     * Translates a list of Total Monthly Sales search results into a list of
     * processable data object
     *
     * @governance 0
     *
     * @param data {search.Result[]} The list of search results to translate
     *
     * @return {MonthlySalesBySkuData[]} List of Monthly Sales data objects
     *
     * @private
     * @function resultsToData
     */
    var resultsToData = R.map(resultToData);

    /**
     * sortChronologically :: [MonthlySalesBySkuData] ->
     * [MonthlySalesBySkuData]
     *
     * Sorts a list of Monthly Sales by SKU data chronologically in ascending
     * order
     *
     * @governance 0
     *
     * @param data {MonthlySalesBySkuData[]} The list to sort
     *
     * @return {MonthlySalesBySkuData[]} The sorted list
     *
     * @private
     * @function sortChronologically
     */
    var sortChronologically = R.sortBy(R.pipe(
        R.prop("month"),
        R.invoker(0, "valueOf")
    ));

    var groupByItem = R.groupBy(R.prop("itemId"));

    var unionByMonth = R.unionWith(
        R.useWith(d.sameMonth, [R.prop("month"), R.prop("month")])
    );

    var fillYear = R.pipe(
        R.converge(unionByMonth, [R.identity, emptyYear]),
        sortChronologically
    );

    var fillYears = R.map(fillYear);

    /**
     * translateResults :: [search.Result] -> [MonthlySalesBySkuData]
     *
     * Translates the given search results to Monthly Sales by SKU data
     *
     * @governance 0
     *
     * @param results {search.Result[]} List of Total Monthly Sales by SKU
     *     search results to translate into processable data Objects. Results
     *     must be grouped by a formulatext column representing the month and
     *     contain a summed quantity column representing the sales for that
     *     month
     *
     * @return {MonthlySalesBySkuData[]} Data list processable by the Total
     *     Monthly by SKU module
     *
     * @static
     * @function translateResults
     */
    var translateResults = R.pipe(
        resultsToData,
        groupByItem,
        fillYears
    );

    /**
     * label :: MonthlySalesBySkuData -> String
     *
     * Generates a label for a MonthlySalesBySkuData object that can be used
     * on, for instance, a chart axis
     *
     * @governance 0
     *
     * @param data {MonthlySalesBySkuData} The data object to generate a label
     *     for
     *
     * @return {String} The label for the given data object
     *
     * @private
     * @function label
     */
    var label = R.pipe(
        R.prop("month"),
        d.format("MMM YYYY")
    );

    /**
     * labels :: [MonthlySalesBySkuData] -> [String]
     *
     * Generates the list of axis labels for the given dataset
     *
     * @governance 0
     *
     * @param data {MonthlySalesBySkuData[]} The dataset to transform to labels
     *
     * @return {String[]} The list of labels to be used for the data
     *
     * @static
     * @function labels
     */
    var labels = R.always(R.pipe(
        sortChronologically,
        R.map(label)
    )(currentRollingYear));

    /**
     * itemGroupToDataset :: [MonthlySalesBySkuData] -> Chart.Dataset
     *
     * Translates the Monthly Sales data for a single SKU into a chartable
     * dataset
     *
     * @governance 0
     *
     * @param data {MonthlySalesBySkuData[]} The Monthly Sales data for a
     *     single SKU
     *
     * @return {Chart.Dataset} Chartable dataset for the item group
     *
     * @private
     * @function itemGroupToDataset
     */
    function itemGroupToDataset(data) {
        return {
            "label": R.pipe(R.head, R.prop("itemName"))(data),
            "data": R.pluck("sales")(data)
        };
    }

    /**
     * datasets :: {k: MonthlySalesBySkuData} -> [Chart.Dataset]
     *
     * Translates an Object of Monthly Sales by SKU data that is grouped by SKU
     * into a list of chartable datasets
     *
     * @governance 0
     *
     * @param data {Object} The Monthly Sales data grouped by SKU
     *
     * @return {Number[]} The values to be used for the chart
     *
     * @static
     * @function datasets
     */
    var datasets = R.pipe(
        R.map(itemGroupToDataset),
        R.values,
        R.sortBy(R.pipe(R.prop("data"), R.sum)),
        // TODO Parameterize this somehow
        R.takeLast(5)
    );

    exports.datasets = datasets;
    exports.labels = labels;
    exports.translateResults = translateResults;
    return exports;
});
