define(["N/ui/serverWidget", "N/search", "N/runtime", "N/file"], function (
    ui, s, runtime, f
) {

    /**
     * Renders the Portlet for Total Monthly Sales by Reseller
     *
     * @exports sa/total-monthly-by-reseller/pl
     *
     * @requires N/ui/serverWidget
     * @requires N/search
     * @requires N/runtime
     * @requires N/file
     *
     * @copyright 2016 Stoic Software
     * @author Eric T Grubaugh <eric@stoic.software>
     *
     * @NApiVersion 2.x
     * @NModuleScope Public
     * @NScriptType Portlet
     */
    var exports = {};

    /**
     * <code>render</code> event handler
     *
     * @governance 10
     *
     * @param context
     *        {Object}
     * @param context.portlet
     *        {Portlet} The portlet object used for rendering.
     * @param context.column
     *        {Number} The column index for the portlet on the
     *            dashboard. Use one of the following numeric values:
     *            <ol>
     *            <li>left column</li>
     *            <li>center column</li>
     *            <li>right column</li>
     *            </ol>
     *
     * @return {void}
     *
     * @static
     * @function render
     */
    function render(context) {
        var p = context.portlet;
        var script = runtime.getCurrentScript();

        p.title = script.getParameter({"name": "custscript_sa_monthlybyreseller_title"});
        p.clientScriptModulePath = "./sa-cl-TotalMonthlyByReseller.js";

        var field = p.addField({
            "id": "custpage_sa_monthlybyreseller",
            "label": "",
            "type": ui.FieldType.INLINEHTML
        });
        field.defaultValue = loadHtml();
    }

    /**
     * Retrieves the HTML for the container that houses the Total Monthly Sales
     * by SKU chart
     *
     * @governance 10
     *
     * @return {String} The HTML for the chart container
     *
     * @private
     * @function loadHtml
     */
    function loadHtml() {
        // TODO Parameterize path for bundle packaging
        return f.load({"id": "SuiteScripts/sku-analytics/total-monthly-by-reseller/TotalMonthlyByReseller.html"})
            .getContents();
    }

    exports.render = render;
    return exports;
});
