define(["../lib/Chart.min.js", "./sa-TotalMonthlyByReseller.js", "N/search"], function (Chart, tm, s) {

    /**
     * Renders the Total Monthly Sales by Reseller chart
     *
     * @exports sa/total-monthly-by-reseller/cl
     *
     * @requires Chart
     * @requires sa/total-monthly-by-reseller
     * @requires N/search
     *
     * @copyright 2016 Stoic Software
     * @author Eric T Grubaugh <eric@stoic.software>
     *
     * @NApiVersion 2.x
     * @NModuleScope Public
     * @NScriptType ClientScript
     */
    var exports = {};

    /**
     * <code>pageInit</code> event handler
     *
     * @governance 10
     *
     * @param context
     *        {Object}
     * @param context.mode
     *        {String} The access mode of the current record. Will be one of
     *            <ul>
     *            <li>copy</li>
     *            <li>create</li>
     *            <li>edit</li>
     *            </ul>
     *
     * @return {void}
     *
     * @static
     * @function pageInit
     */
    function pageInit(context) { // eslint-disable-line no-unused-vars
        // TODO Parameterize search ID
        draw(
            "#sa-monthly-by-reseller",
            tm.translateResults(skuData("customsearch_sa_monthlybyreseller"))
        );
    }

    /**
     * Retrieves Total Monthly Sales by Reseller data
     *
     * @governance 10
     *
     * @param searchId {Number} The internal ID of the Total Monthly Sales
     *     Saved Search to execute
     * @param [count=1000] {Number} The number of results to retrieve
     *
     * @return {search.Result[]} Total Monthly Sales search results
	 *
	 * @private
	 * @function skuData
     */
    function skuData(searchId, count) {
        return s.load({"id": searchId})
            .run()
            .getRange({
                "start": 0,
                "end": count || 1000
            });
    }

    /**
     * Draws the Total Monthly Sales by Reseller chart
     *
     * @governance 0
     *
     * @param selector {String} The HTML selector for the appropriate canvas
     *     element that will contain the chart
     * @param data {MonthlySalesData[]} Total Monthly Sales data structure
     *
     * @return {void}
     *
     * @private
     * @function draw
     */
    function draw(selector, data) {
        new Chart(jQuery(selector)[0].getContext("2d"), {
            "type": "bar",
            "data": {
                "labels": tm.labels(data),
                "datasets": tm.datasets(data)
            },
            "options": {
                "legend": {
                    "display": false,
                    "position": "bottom",
                    "fullWidth": false
                },
                "scales": {
                    "yAxes": [{
                        "ticks": {
                            "beginAtZero": true
                        }
                    }]
                },
                "responsive": false,
                "maintainAspectRatio": false
            }
        });
    }

    exports.pageInit = pageInit;
    return exports;
});
