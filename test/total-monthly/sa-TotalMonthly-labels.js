var R = require("ramda");
var moment = require("moment");
var tm = require("../../FileCabinet/SuiteScripts/sku-analytics/total-monthly/sa-TotalMonthly.js");

// Load test data and convert months to moment instances
var annualData = R.map(function (n) {
    return {
        "month": moment(n.month),
        "sales": n.sales
    };
})(require("./annualData.json"));

describe("sa-TotalMonthly.js", function () {
    describe("#labels(data)", function () {
        describe("Invalid data tests", function () {
            context("When an empty Array is given", function () {
                it("should return an empty list", function () {
                    tm.labels([]).should.be.empty();
                });
            });
        });
        describe("Valid data tests", function () {
            context("When data for one year is given", function () {
                it("should return the month names for one year", function () {
                    var expected = [
                        "Jan 2016", "Feb 2016", "Mar 2016", "Apr 2016", "May 2016", "Jun 2016",
                        "Jul 2016", "Aug 2016", "Sep 2016", "Oct 2016", "Nov 2016",
                        "Dec 2016"
                    ];
                    tm.labels(annualData).should.eql(expected);
                });
            });
        });
    });
});
