var tm = require("../../FileCabinet/SuiteScripts/sku-analytics/total-monthly/sa-TotalMonthly.js");
var annualData = require("./annualData.json");

describe("sa-TotalMonthly.js", function () {
    describe("#values(data)", function () {
        describe("Invalid data tests", function () {
            context("When an empty Array is given", function () {
                it("should return an empty list", function () {
                    tm.values([]).should.be.empty();
                });
            });
        });
        describe("Valid data tests", function () {
            context("When data for one year is given", function () {
                it("should return the sales for one year", function () {
                    var expected = [50, 48, 37, 60, 35, 38, 12, 15, 28, 33, 79, 65];
                    tm.values(annualData).should.eql(expected);
                });
            });
        });
    });
});
